// To fix problems between "bootstrap" and "react-hook-form"
type RBRef = string & ((ref: Element | null) => void);
