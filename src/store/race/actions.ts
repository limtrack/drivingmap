import {
  DISJOIN_PLAYER_ASYNC,
  JOIN_PLAYER_ASYNC,
  SET_RACE,
  RESET_RACE,
  SET_RACE_ASYNC,
  RaceActionTypes,
} from './types';

export function DisjoinPlayerAsync(payload: object): RaceActionTypes {
  return {
    type: DISJOIN_PLAYER_ASYNC,
    payload,
  };
}

export function JoinPlayerAsync(payload: object): RaceActionTypes {
  return {
    type: JOIN_PLAYER_ASYNC,
    payload,
  };
}

export function resetRace(): RaceActionTypes {
  return {
    type: RESET_RACE,
  };
}

export function setRace(payload: object): RaceActionTypes {
  return {
    type: SET_RACE,
    payload,
  };
}

export function setRaceAsync(payload: object): RaceActionTypes {
  return {
    type: SET_RACE_ASYNC,
    payload,
  };
}
