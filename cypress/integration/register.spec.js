context('User login view', () => {
  beforeEach(() => {
    cy.visit('http://localhost:8080/#/user-login');
  });

  it('format no correct in email input', () => {
    cy.get('form input[type="email"]')
      .type('fake-thing')
      .should('have.value', 'fake-thing')
      .and('have.class', 'is-invalid');
  });

  it('action submit button is disabled meanwhile the inputs are incorrect', () => {
    cy.get('form input[type="email"]')
      .type('fake-think')
      .should('have.value', 'fake-think')
      .and('have.class', 'is-invalid');

    cy.get('form button[type="submit"]').should('have.class', 'disabled');
  });

  it('action submit button is enabled when the inputs are correct', () => {
    cy.get('form input[type="email"]')
      .type('fake@domain.com')
      .should('have.value', 'fake@domain.com')
      .and('not.have.class', 'is-invalid');

    cy.get('form input[type="password"]')
      .type('123456')
      .should('have.value', '123456')
      .and('not.have.class', 'is-invalid');

    cy.get('form button[type="submit"]').and('not.have.class', 'disabled');
  });

  it('alert message appears if login is incorrect', () => {
    cy.get('form input[type="email"]')
      .type('fake@domain.com')
      .should('have.value', 'fake@domain.com')
      .and('not.have.class', 'is-invalid');

    cy.get('form input[type="password"]')
      .type('123456')
      .should('have.value', '123456')
      .and('not.have.class', 'is-invalid');

    cy.get('form button[type="submit"]').click();

    cy.wait(1000)
      .get('.alert.alert-dismissible')
      .should('have.class', 'alert-warning');
  });
});
