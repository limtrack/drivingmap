import React, { useEffect } from 'react';
// Redux
import { useSelector } from 'react-redux';
// Types
import { StateType } from '../../../store/state/types';
// Actions
import * as ModalActions from '../../../store/modal/actions';
// Hooks
import useActions from '../../hooks/UseActions';
// Components
import LeftMenu from '../../features/LeftMenu';
import RacesList from '../../features/RacesList';
import RaceInfo from '../../features/RaceInfo';
import UserForm from '../../features/UserForm';
// Transitions css
import { CSSTransition } from 'react-transition-group';

const AppMain: React.FC = (): JSX.Element | null => {
  // State
  const user = useSelector((state: StateType) => state.user);
  const race = useSelector((state: StateType) => state.race);
  // Actions
  const modalActions = useActions(ModalActions);
  // others
  const userId =
    typeof user.id === 'undefined' || user.id === null || user.id === ''
      ? false
      : true;

  // Load modal with login formulary
  useEffect(() => {
    if (!userId) {
      setTimeout(() => {
        modalActions.setModal({
          title: 'Create your player',
          content: UserForm,
          backdrop: 'static',
          showCloseButton: false,
          showFooter: false,
          visible: true,
        });
      }, 1000);
    }
  }, [userId, modalActions]);

  return userId ? (
    <CSSTransition in={userId} timeout={500} classNames="main-transition">
      <div className="main">
        <div className="left-menu h-100 p-3">
          <LeftMenu />
        </div>
        <div className="main__left-column h-100 p-3">
          <RacesList race={race} user={user} />
        </div>
        <div className="main__right-column h-100">
          <RaceInfo race={race} user={user} />
        </div>
      </div>
    </CSSTransition>
  ) : null;
};

export default AppMain;
