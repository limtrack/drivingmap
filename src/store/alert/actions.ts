import { SET_ALERT, RESET_ALERT, AlertActionTypes } from './types';

// TypeScript infers that this function is returning ResetAlertAction
export function resetAlert(): AlertActionTypes {
  return {
    type: RESET_ALERT,
  };
}

// TypeScript infers that this function is returning SetAlertAction
export function setAlert(payload: object): AlertActionTypes {
  return {
    type: SET_ALERT,
    payload,
  };
}
