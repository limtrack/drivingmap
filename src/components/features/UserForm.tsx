import React, { useState, useEffect } from 'react';
// Redux
import { useSelector } from 'react-redux';
// Types
import { StateType } from '../../store/state/types';
// Hooks
import { useForm } from 'react-hook-form';
import useActions from '../hooks/UseActions';
// Actions
import * as ModalActions from '../../store/modal/actions';
import * as UserActions from '../../store/user/actions';
// Bootstrap
import Form from 'react-bootstrap/Form';
// Components
import AppButtonLoading from '../ui/interface/AppButtonLoading';
import AppCharacterPlayer from '../ui/interface/AppCharacterPlayer';
// Data
import characters from '../../data/characters.js';

const UserForm: React.FC = (): JSX.Element => {
  // state
  const user = useSelector((state: StateType) => state.user);
  const [isLoading, setLoading] = useState(false);
  // actions
  const modalActions = useActions(ModalActions);
  const userActions = useActions(UserActions);
  // form validator
  const { handleSubmit, errors, register } = useForm();

  // Hide modal when user change
  useEffect(() => {
    if (user.id) {
      modalActions.setModal({ visible: false });
    }
  }, [user.id, modalActions]);

  /**
   * handle submit form
   */
  const onSubmit = (data: object): void => {
    setLoading(true);
    userActions.setUserAsync(data);
  };

  return (
    <Form onSubmit={handleSubmit(onSubmit)}>
      <Form.Group controlId="formName">
        <Form.Label>Name</Form.Label>
        <Form.Control
          ref={
            register({
              required: {
                value: true,
                message: 'You must have a name',
              },
              minLength: {
                value: 3,
                message: 'The field need at least 3 characters',
              },
            }) as RBRef
          }
          isInvalid={!!errors.name}
          name="name"
          placeholder="Your 'cool' name"
          type="text"
        />
        <Form.Text className="text-muted">
          This is the name that everybody will see.
        </Form.Text>
        {/* Show errors */}
        {errors.name && (
          <Form.Control.Feedback type="invalid">
            {(errors['name'] as any)?.message}
          </Form.Control.Feedback>
        )}
      </Form.Group>

      <Form.Group controlId="formCharacter">
        <Form.Label>Select your character</Form.Label>
        {characters.map((character: any, index: number) => {
          return (
            <Form.Check
              custom
              id={`character${character.id}`}
              key={character.id}
              className="characters-radio"
              name="character"
              value={character.id}
              type="radio"
            >
              <Form.Check.Input
                value={character.id}
                isInvalid={!!errors.character}
                name="character"
                type="radio"
                ref={
                  register({
                    required: {
                      value: true,
                      message: 'Select your character',
                    },
                  }) as RBRef
                }
              />
              <Form.Check.Label className="check-label">
                <AppCharacterPlayer characterId={character.id} width={28} />
                <span className="check-label__text">{character.label}</span>
              </Form.Check.Label>
              {/* Show errors */}
              {index === characters.length - 1 && errors.character && (
                <Form.Control.Feedback type="invalid">
                  {(errors['character'] as any)?.message}
                </Form.Control.Feedback>
              )}
            </Form.Check>
          );
        })}
      </Form.Group>

      <AppButtonLoading
        isLoading={isLoading}
        label="Create"
        labelLoading="Creating..."
      />
    </Form>
  );
};

export default UserForm;
