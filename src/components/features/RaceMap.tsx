import React, { useState, useMemo } from 'react';
// Contants
import {
  COUNTDOWN_RACE,
  RACE_STATUS_RUNNING,
  RACE_STATUS_FINISHED,
} from '../../constants';
// Models
import { RaceType } from '../../lib/firebase/models/race';
// Components
import AppCountDown from '../ui/features/AppCountDown';
import RaceGoogleMap from './RaceGoogleMap';
import RaceMapPath from './RaceMapPath';
import RaceMapPlayers from './RaceMapPlayers';
import RaceFinish from './RaceFinish';
// Types
type Props = {
  race: RaceType;
};

const RaceMap: React.FC<Props> = ({ race }: Props): JSX.Element => {
  // states
  const [googleMap, setGoogleMap] = useState();
  // memos
  const memoCoordinates = useMemo(() => {
    return race.coordinatesKeys;
  }, [race.coordinatesKeys]);
  const seconds = useMemo(() => {
    return race.date
      ? Math.trunc(race.date.seconds - new Date().getTime() / 1000)
      : COUNTDOWN_RACE;
  }, [race]);

  return (
    <>
      {/* Map google */}
      <RaceGoogleMap onLoadMap={setGoogleMap} />
      {/* CountDown */}
      {race.status === RACE_STATUS_RUNNING ? (
        <AppCountDown
          className="over-layer"
          seconds={seconds}
          title="The race starts in..."
        />
      ) : null}
      {/* Map path */}
      {googleMap && (
        <RaceMapPath gMap={googleMap} coordinates={memoCoordinates} />
      )}
      {/* Map markers (players) */}
      {googleMap && <RaceMapPlayers gMap={googleMap} race={race} />}
      {/* Race is finished */}
      {race.status === RACE_STATUS_FINISHED && <RaceFinish race={race} />}
    </>
  );
};

export default RaceMap;
