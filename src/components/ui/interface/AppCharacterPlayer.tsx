import React from 'react';
// Data
import characters from '../../../data/characters.js';

// Types
type Props = {
  characterId: string | undefined;
  width?: number;
};

type Character = {
  id: string;
  image: string;
  label: string;
};

const AppCharacterPlayer: React.FC<Props> = ({
  characterId,
  width = 68,
}: Props): JSX.Element | null => {
  const characterData = characters.find((character: Character): boolean => {
    return character.id === characterId;
  });

  return characterData ? (
    <img
      className="character-image"
      width={width}
      src={characterData.image}
      alt={characterData.label}
    />
  ) : null;
};

export default AppCharacterPlayer;
