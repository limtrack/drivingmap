import React, { useEffect, useState, useCallback } from 'react';
// Constants
import { RACE_STATUS_FINISHED } from '../../constants';
// Models
import { watchNewRaces, RaceType } from '../../lib/firebase/models/race';
import { PlayerType } from '../../lib/firebase/models/player';
// Hooks
import useActions from '../hooks/UseActions';
// Actions
import * as ModalActions from '../../store/modal/actions';
import * as RaceActions from '../../store/race/actions';
// Components
import RaceForm from './RaceForm';
import AppListItems from '../ui/features/AppListItems';
import RaceItem from './RaceItem';
// Font awesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// Bootstrap
import Button from 'react-bootstrap/Button';

// Types
type Props = {
  race: RaceType;
  user: PlayerType;
};

const RacesList: React.FC<Props> = ({ user, race }: Props): JSX.Element => {
  // state
  const [races, setRaces] = useState([]);
  // Actions
  const modalActions = useActions(ModalActions);
  const raceActions = useActions(RaceActions);
  // Handler to watch new races
  const handlerWatchNewRaces = useCallback((querySnapshot: any): void => {
    !querySnapshot.empty &&
      setRaces((): never[] => {
        return querySnapshot.docs.reduce((sumRaces: RaceType[], r: any) => {
          const dataRace = r.data();
          if (dataRace.status !== RACE_STATUS_FINISHED) {
            sumRaces.push({
              id: r.id,
              ...dataRace,
            });
          }

          return sumRaces;
        }, []);
      });
  }, []);
  // Show modal races formulary
  const modalRacesForm = useCallback((): void => {
    modalActions.setModal({
      title: 'Create your race',
      content: RaceForm,
      showFooter: false,
      visible: true,
    });
  }, []);
  // Toggle join race
  const toggleJoinRace = useCallback(
    (data: RaceType): void => {
      const allPlayers = data.players;
      const isUserInRace = allPlayers.find((player) => {
        return player.id === user.id;
      });

      if (data.id !== race.id) {
        if (!isUserInRace) {
          // Join user to race
          allPlayers.push({
            id: user.id,
            name: user.name,
            character: user.character,
            movements: 0,
          });
        }
        raceActions.JoinPlayerAsync(
          Object.assign(data, { players: allPlayers }),
        );
      } else {
        // Remove user from race
        raceActions.DisjoinPlayerAsync(
          Object.assign(data, {
            players: allPlayers.filter((player) => {
              return player.id !== user.id;
            }),
          }),
        );
      }
    },
    [user.id, race.id],
  );
  // Show action button in title
  const handlerTitleAction = useCallback(() => {
    return (
      <Button
        disabled={Boolean(race.id)}
        variant="primary"
        onClick={modalRacesForm}
      >
        <FontAwesomeIcon icon="plus" />
      </Button>
    );
  }, [race]);

  // Subscribe / unsubscribe to watch new races
  useEffect(() => {
    const idSubscribe = watchNewRaces().onSnapshot(handlerWatchNewRaces);
    return (): void => {
      idSubscribe();
    };
  }, []);

  return (
    <AppListItems
      data={races}
      title="Races"
      titleAction={handlerTitleAction}
      subtitle="Already created"
      render={(item: any): any => {
        return (
          <RaceItem
            item={item}
            itemSelected={race}
            handleClick={toggleJoinRace}
          />
        );
      }}
    />
  );
};

export default RacesList;
