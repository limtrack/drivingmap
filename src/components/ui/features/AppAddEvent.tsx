import React, { useEffect } from 'react';

// Types
type Props = {
  context: any;
  event: string;
  handlerEvent: (e: any) => void;
};

const AppAddEvent: React.FC<Props> = ({
  context,
  event,
  handlerEvent,
}: Props): JSX.Element => {
  useEffect(() => {
    context.addEventListener(event, handlerEvent, false);
    return (): void => {
      context.removeEventListener(event, handlerEvent);
    };
  }, [context, handlerEvent, event]);

  return <></>;
};

export default React.memo(AppAddEvent);
