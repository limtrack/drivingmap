// Types
import { SET_USER } from '../user/types';
import { SET_ALERT } from '../alert/types';
// Models
import { newPlayer } from '../../lib/firebase/models/player';
// Saga
import { takeLatest, put } from 'redux-saga/effects';

/**
 * Set new user data in server (firebase)
 */
function* setUserDataAsync({ payload }: any): any {
  try {
    // Save in server
    const userData = yield newPlayer(payload);

    // Change state in store
    yield put({
      type: SET_USER,
      payload: { id: userData.id, ...payload },
    });
  } catch (err) {
    // Show error
    yield put({
      type: SET_ALERT,
      payload: {
        text: err.message || 'There was an error trying create a new user',
        type: 'danger',
      },
    });
  }
}

// Watcher functions
export function* watchSetUserData(): any {
  yield takeLatest('SET_USER_ASYNC', setUserDataAsync);
}
