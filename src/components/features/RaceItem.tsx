import React from 'react';
// Contants
import { RACE_STATUS_RUNNING } from '../../constants';
// Models
import { RaceType } from '../../lib/firebase/models/race';
// Font awesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

// Types
type Props = {
  item: RaceType;
  itemSelected: RaceType;
  handleClick: (item: any) => void;
};

const RaceItem: React.FC<Props> = ({
  item,
  itemSelected,
  handleClick,
}: Props): JSX.Element => {
  const noAction = (): boolean => {
    return false;
  };

  return (
    <li
      className={`items__list__item ${
        item.id === itemSelected.id
          ? 'items__list__item items__list__item--selected'
          : ''
      } ${
        item.status === RACE_STATUS_RUNNING
          ? 'items__list__item items__list__item--running'
          : ''
      }`}
    >
      <div className="items__list__item-left">
        <span className="items__list__item-name">
          {item.fromCity} &rarr; {item.toCity}
        </span>
        <span className="items__list__item-info">
          <strong>{item.distanceBetweenCities}</strong> km&nbsp; - &nbsp;
          <strong>{item.players.length}</strong> players
        </span>
      </div>
      <div
        className={`items__list__item-right ${
          item.status === RACE_STATUS_RUNNING
            ? 'items__list__item-right--disabled'
            : ''
        }`}
        onClick={
          item.status !== RACE_STATUS_RUNNING
            ? handleClick.bind(null, item)
            : noAction
        }
      >
        <FontAwesomeIcon
          icon={
            item.status === RACE_STATUS_RUNNING
              ? 'ellipsis-h'
              : item.id === itemSelected.id
              ? 'sign-out-alt'
              : 'sign-in-alt'
          }
          className="items__list__item__action-icon"
        />
      </div>
    </li>
  );
};

export default RaceItem;
