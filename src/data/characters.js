const characters = [
  {
    id: 'mario',
    image: './images/mario.png',
    label: 'Mario',
  },
  {
    id: 'luigi',
    image: './images/luigi.png',
    label: 'Luigi',
  },
  {
    id: 'bowser',
    image: './images/bowser.png',
    label: 'Bowser',
  },
  {
    id: 'peach',
    image: './images/peach.png',
    label: 'Peach',
  },
];

export default characters;
