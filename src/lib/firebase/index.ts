// Firebase
import firebase from 'firebase/app';
import 'firebase/firestore';
// Contants
import { FIREBASE_CONFIG } from '../../constants';

// Init
firebase.initializeApp(FIREBASE_CONFIG);

export const fs = firebase.firestore(); // Reference to Firestore
