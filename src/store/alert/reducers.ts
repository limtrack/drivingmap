// Types
import { SET_ALERT, RESET_ALERT, AlertActionTypes } from './types';
// State
import initialState from '../state/initialState';

const { alert } = initialState;

export default (state: object = alert, action: AlertActionTypes): object => {
  switch (action.type) {
    case RESET_ALERT: {
      return { ...alert };
    }
    case SET_ALERT: {
      return {
        ...state,
        ...action.payload,
      };
    }
    default: {
      return state;
    }
  }
};
