export const SET_MODAL = 'SET_MODAL';
export const RESET_MODAL = 'RESET_MODAL';

interface ResetModalAction {
  type: typeof RESET_MODAL;
}

interface SetModalAction {
  type: typeof SET_MODAL;
  payload: object;
}

export type ModalActionTypes = ResetModalAction | SetModalAction;
