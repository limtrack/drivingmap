import React from 'react';
// Bootstrap
import Button, { ButtonProps } from 'react-bootstrap/Button';
import Spinner from 'react-bootstrap/Spinner';

// Types
type Props = {
  isLoading: boolean;
  label?: string;
  labelLoading?: string;
} & ButtonProps;

const AppButtonLoading: React.FC<Props> = ({
  isLoading = false,
  label = 'Submit',
  labelLoading = 'Loading...',
  type = 'submit',
  ...props
}: Props): JSX.Element => {
  return (
    <Button disabled={isLoading} type={type} {...props}>
      {isLoading && (
        <Spinner
          as="span"
          animation="border"
          className="mr-2"
          size="sm"
          role="status"
          aria-hidden="true"
        />
      )}
      {isLoading ? labelLoading : label}
    </Button>
  );
};

export default AppButtonLoading;
