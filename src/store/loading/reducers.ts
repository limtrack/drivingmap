// Types
import { SET_LOADING, RESET_LOADING, LoadingActionTypes } from './types';
// State
import initialState from '../state/initialState';

const { loading } = initialState;

export default (
  state: object = loading,
  action: LoadingActionTypes,
): object => {
  switch (action.type) {
    case RESET_LOADING: {
      return { ...loading };
    }
    case SET_LOADING: {
      return {
        ...state,
        ...action.payload,
      };
    }
    default: {
      return state;
    }
  }
};
