export const SET_ALERT = 'SET_ALERT';
export const RESET_ALERT = 'RESET_ALERT';

interface ResetAlertAction {
  type: typeof RESET_ALERT;
}

interface SetAlertAction {
  type: typeof SET_ALERT;
  payload: object;
}

export type AlertActionTypes = ResetAlertAction | SetAlertAction;
