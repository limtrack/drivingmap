import { SET_USER, SET_USER_ASYNC, RESET_USER, UserActionTypes } from './types';

// TypeScript infers that this function is returning ResetUserAction
export function resetUser(): UserActionTypes {
  return {
    type: RESET_USER,
  };
}

// TypeScript infers that this function is returning SetUserAction
export function setUser(payload: object): UserActionTypes {
  return {
    type: SET_USER,
    payload,
  };
}

// TypeScript infers that this function is returning SetUserAction
export function setUserAsync(payload: object): UserActionTypes {
  return {
    type: SET_USER_ASYNC,
    payload,
  };
}
