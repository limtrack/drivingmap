import React from 'react';
// Models
import { RaceType, RacePlayer } from '../../lib/firebase/models/race';
// Components
import AppCharacterPlayer from '../ui/interface/AppCharacterPlayer';
// Hooks
import useActions from '../hooks/UseActions';
// Actions
import * as RaceActions from '../../store/race/actions';
// Bootstrap
import Button from 'react-bootstrap/Button';

// Types
type Props = {
  race: RaceType;
};

const RaceFinish: React.FC<Props> = ({ race }: Props): JSX.Element | null => {
  const raceActions = useActions(RaceActions);
  const playersOrdered = race.players.sort((a: RacePlayer, b: RacePlayer) => {
    return b.movements - a.movements;
  });

  return race ? (
    <div className="race-finish">
      <h3 className="race-finish__title">Ranking race</h3>
      <ul className="items_list race-finish__list">
        {playersOrdered.map((player: RacePlayer, index: number) => {
          return (
            <li key={player.id} className="items__list__item">
              <div className="items__list__item-left items__list__item-left--players">
                <AppCharacterPlayer characterId={player.character} width={38} />
                <span className="items__list__item-name">{player.name}</span>
              </div>
              <div className="items__list__item-right">
                <span className="items__list__item-position">{index + 1}</span>
              </div>
            </li>
          );
        })}
      </ul>
      <Button variant="primary" onClick={raceActions.resetRace}>
        Run other race
      </Button>
    </div>
  ) : null;
};

export default RaceFinish;
