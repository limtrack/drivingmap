import React from 'react';
// Contants
import { GOOGLE_MAP_API } from './constants';
// Redux
import { Provider } from 'react-redux';
import { compose } from 'redux';
import storeSetup from './store';
import { PersistGate } from 'redux-persist/integration/react';
// Components
import AppHeader from './components/ui/layout/AppHeader';
import AppMain from './components/ui/layout/AppMain';
import AppModal from './components/ui/interface/AppModal';
import AppAlert from './components/ui/interface/AppAlert';
import AppLoading from './components/ui/interface/AppLoading';
import GMLoader from './components/hoc/GMLoader';
import SizeScreenMinimum from './components/hoc/SizeScreenMinimum';
// Bootstrap
import Container from 'react-bootstrap/Container';
// FontAwesome
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faUserCircle,
  faCode,
  faSignInAlt,
  faSignOutAlt,
  faTimesCircle,
  faEllipsisH,
  faPlus,
  faFlagCheckered,
  faArrowAltCircleLeft,
  faArrowAltCircleRight,
  faArrowAltCircleUp,
  faArrowAltCircleDown,
} from '@fortawesome/free-solid-svg-icons';

library.add(
  faUserCircle,
  faCode,
  faSignInAlt,
  faSignOutAlt,
  faTimesCircle,
  faEllipsisH,
  faPlus,
  faFlagCheckered,
  faArrowAltCircleLeft,
  faArrowAltCircleRight,
  faArrowAltCircleUp,
  faArrowAltCircleDown,
);

const { store, persistor } = storeSetup;

const App: React.FC = (): JSX.Element => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <div className="app h-100">
          <Container className="d-flex flex-column justify-content-center h-100">
            {/* Header */}
            <AppHeader />
            {/* Main */}
            <AppMain />
          </Container>
          {/* Modal */}
          <AppModal />
          {/* Alert */}
          <AppAlert />
          {/* Loading */}
          <AppLoading />
        </div>
      </PersistGate>
    </Provider>
  );
};

export default compose<any>(
  GMLoader({ apiKey: GOOGLE_MAP_API }),
  SizeScreenMinimum({ width: 1200, height: 750 }),
)(App);
