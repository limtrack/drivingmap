// constants
import { REDUX_KEY } from '../constants';
// redux
import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
// redux-persist
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
// reducers
import rootReducer from './root-reducers';
// saga
import rootSaga from './sagas';

// Persist
const persistConfig = {
  key: REDUX_KEY,
  storage,
};
const persistedReducer = persistReducer(persistConfig, rootReducer);
// Saga
const sagaMiddleware = createSagaMiddleware();
// Store
const store = createStore(persistedReducer, applyMiddleware(sagaMiddleware));
// Persist
const persistor = persistStore(store);

// Run saga
sagaMiddleware.run(rootSaga);

export default {
  store,
  persistor,
};
