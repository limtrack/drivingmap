import React from 'react';
// Redux
import { useSelector } from 'react-redux';
// Types
import { StateType } from '../../../store/state/types';
// Bootstrap
import Spinner from 'react-bootstrap/Spinner';

const AppLoading: React.FC = (): JSX.Element | null => {
  const loading = useSelector((state: StateType) => state.loading);

  return loading.visible ? (
    <div className="app-loading d-flex flex-column justify-content-center align-items-center w-100 h-100">
      <Spinner animation="border" className="mb-2" />
      <span className="app-loading__text">{loading.text || 'Loading'}</span>
    </div>
  ) : null;
};

export default AppLoading;
