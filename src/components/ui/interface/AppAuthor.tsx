import React from 'react';

const AppAuthor: React.FC = (): JSX.Element | null => {
  return (
    <div className="info-text">
      <p>
        Hello my name is <b>Antonio Irlandés García</b> and I am FrontEnd
        Development..
      </p>
      <p>
        <b>DrivingMap</b> is an online mini-game that I did to learn more about
        the differents features that ReactJs has.
      </p>
      <p>
        If you are more interested in me like developer you can find more about
        me in my{' '}
        <a
          href="http://www.programador-frontend.com"
          rel="noopener noreferrer"
          target="_blank"
        >
          website
        </a>
        .
      </p>
      <p>Nothing more to say you, receives a warm greeting from me.</p>
    </div>
  );
};

export default AppAuthor;
