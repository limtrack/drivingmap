// Firebase
import { fs } from '../index';

// Model
export interface PlayerType {
  id?: string | undefined;
  character: string;
  name: string;
  victories: number;
}

const defaultData = {
  character: '', // character selected
  name: 'No name',
  victories: 0,
};

// name collection in firebase
const nameCollection = 'players';

/**
 * Get the data model
 */
export const getDataModel = (): PlayerType => {
  return defaultData;
};

/**
 * Get user data by id
 */
export const getPlayerById = async (id: string): Promise<any> => {
  return await fs
    .collection(nameCollection)
    .doc(id)
    .get();
};

/**
 * Create a new player in the database (only used in register)
 */
export const newPlayer = async (data: object = {}): Promise<any> => {
  const currentData = Object.assign({}, getDataModel(), data);
  return await fs.collection(nameCollection).add(currentData);
};
