import { combineReducers } from 'redux';
// models
import user from './user/reducers';
import race from './race/reducers';
// UI
import loading from './loading/reducers';
import modal from './modal/reducers';
import alert from './alert/reducers';

const rootReducer = combineReducers({
  // models
  user,
  race,
  // UI
  alert,
  loading,
  modal,
});

export default rootReducer;
