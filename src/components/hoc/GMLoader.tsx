import React, { useState, useEffect } from 'react';
// google maps
import { Loader } from 'google-maps';
// Types
type Props = {
  apiKey: string;
};

const GMLoader = ({ apiKey = '', ...props }: Props): any => (
  WrappedComponent: any,
): React.FC => {
  const GML: React.FC = (): JSX.Element => {
    const [googleMapsLoader, setGoogleMapsLoader] = useState(false);

    useEffect(() => {
      if (!window.google) {
        // Using an IIFE
        (async (): Promise<void> => {
          const loader = new Loader(apiKey);

          await loader.load();
          setGoogleMapsLoader(true);
        })();
      }
    });

    return (
      <>
        {googleMapsLoader === false ? (
          <div className="error-message-full">Loading Google Maps...</div>
        ) : (
          <WrappedComponent {...props} />
        )}
      </>
    );
  };

  return GML;
};

export default GMLoader;
