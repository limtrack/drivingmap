export const SET_LOADING = 'SET_LOADING';
export const RESET_LOADING = 'RESET_LOADING';

interface ResetLoadingAction {
  type: typeof RESET_LOADING;
}

interface SetLoadingAction {
  type: typeof SET_LOADING;
  payload: object;
}

export type LoadingActionTypes = ResetLoadingAction | SetLoadingAction;
