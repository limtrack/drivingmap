import { SET_LOADING, RESET_LOADING, LoadingActionTypes } from './types';

// TypeScript infers that this function is returning ResetLoadingAction
export function resetLoading(): LoadingActionTypes {
  return {
    type: RESET_LOADING,
  };
}

// TypeScript infers that this function is returning SetLoadingAction
export function setLoading(payload: object): LoadingActionTypes {
  return {
    type: SET_LOADING,
    payload,
  };
}
