// Types
import { SET_MODAL, RESET_MODAL, ModalActionTypes } from './types';
// State
import initialState from '../state/initialState';

const { modal } = initialState;

export default (state: object = modal, action: ModalActionTypes): object => {
  switch (action.type) {
    case RESET_MODAL: {
      return { ...modal };
    }
    case SET_MODAL: {
      return {
        ...state,
        ...action.payload,
      };
    }
    default: {
      return state;
    }
  }
};
