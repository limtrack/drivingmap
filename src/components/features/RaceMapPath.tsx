import React, { useEffect, useState } from 'react';
// Models
import { CoordinatesKey } from '../../lib/firebase/models/race';

type Props = {
  gMap: any;
  coordinates: CoordinatesKey[];
};

const RaceMapPath: React.FC<Props> = ({
  gMap,
  coordinates,
}: Props): JSX.Element => {
  // state
  const [polylinePath, setPolylinePath] = useState<any>(null);
  // draw path
  const drawPolyline = (): void => {
    const polyline = new google.maps.Polyline({
      path: coordinates.map((coordinate: any) => {
        return {
          lat: coordinate.lat,
          lng: coordinate.lng,
        };
      }),
      strokeColor: '#3d60ff',
      strokeOpacity: 0.7,
      strokeWeight: 6,
    });

    polyline.setMap(gMap);
    setPolylinePath(polyline);
  };
  // clean path
  const cleanPath = (): void => {
    polylinePath && polylinePath.setMap(null);
  };

  useEffect(() => {
    cleanPath();
    drawPolyline();
  }, [coordinates]);

  return <></>;
};

export default React.memo(RaceMapPath);
