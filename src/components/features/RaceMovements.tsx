import React, { useState, useEffect, useCallback, useMemo } from 'react';
// Contants
import {
  COUNTDOWN_RACE,
  KEYBOARD_KEYS,
  NUM_MAX_MOVEMENTS,
  RACE_STATUS_PENDING,
  RACE_STATUS_RUNNING,
  RACE_STATUS_FINISHED,
  REQUEST_BY_MOVEMENTS,
} from '../../constants';
// Models
import { RaceType } from '../../lib/firebase/models/race';
import { PlayerType } from '../../lib/firebase/models/player';
// Components
import AppCountDown from '../ui/features/AppCountDown';
import AppAddEvent from '../ui/features/AppAddEvent';
import AppKeyboardKeys from '../ui/features/AppKeyboardKeys';
// Hooks
import useActions from '../hooks/UseActions';
// Actions
import * as RaceActions from '../../store/race/actions';
import * as UserActions from '../../store/user/actions';

// Types
type Props = {
  race: RaceType;
  user: PlayerType;
};

const RaceMovements: React.FC<Props> = ({ race, user }: Props): JSX.Element => {
  // state
  const [movementNumber, setMovementNumber] = useState(0);
  const [currentKey, setCurrentKey] = useState<string>();
  const [showKeys, setShowKeys] = useState(false);
  const [resultAction, setResultAction] = useState('');
  // actions
  const raceActions = useActions(RaceActions);
  const userActions = useActions(UserActions);
  // const
  const coordinatesKeysMemo = useMemo(() => {
    return race.coordinatesKeys;
  }, [race.coordinatesKeys]);
  const windowMemo = useMemo(() => {
    return window;
  }, []);
  const seconds = useMemo(() => {
    return race.date
      ? Math.trunc(race.date.seconds - new Date().getTime() / 1000)
      : COUNTDOWN_RACE;
  }, [race]);
  // handler "countdown" (function)
  const handlerCountDown = (): void => {
    setShowKeys(true);
  };
  // handler event "keydown"
  const handlerKeyDown = useCallback(
    (e: any): void => {
      // step
      const stepMovement =
        coordinatesKeysMemo.length > NUM_MAX_MOVEMENTS
          ? Math.ceil(coordinatesKeysMemo.length / NUM_MAX_MOVEMENTS)
          : coordinatesKeysMemo.length;
      // key pressed
      const keyMatched = KEYBOARD_KEYS.find((k) => {
        return k.key === e.key;
      });

      if (race.status !== RACE_STATUS_FINISHED && keyMatched) {
        // Right key pressed
        if (coordinatesKeysMemo[movementNumber].key === keyMatched.key) {
          setResultAction('right');

          // next movement (number)
          const nextMovement =
            movementNumber * stepMovement >= coordinatesKeysMemo.length
              ? coordinatesKeysMemo.length - 1
              : movementNumber * stepMovement;
          const updatesRace: {
            status?: string;
            winningPlayer?: string;
            players?: any[];
          } = {};

          // Finish race
          if (nextMovement === coordinatesKeysMemo.length - 1) {
            Object.assign(updatesRace, {
              status: RACE_STATUS_FINISHED,
              winningPlayer: user.id,
            });
          }

          // Add movements to player (user)
          if (
            (movementNumber > 0 &&
              movementNumber % REQUEST_BY_MOVEMENTS === 0) ||
            nextMovement === coordinatesKeysMemo.length - 1
          ) {
            Object.assign(updatesRace, {
              players: race.players.map((player) => {
                if (player.id === user.id) {
                  player.movements = nextMovement;
                }
                return player;
              }),
            });
          }

          // Update data race
          if (Object.keys(updatesRace).length) {
            raceActions.setRaceAsync(Object.assign(race, updatesRace));
            // add victory to user
            if (updatesRace.winningPlayer) {
              userActions.setUserAsync(
                Object.assign(user, { victories: user.victories + 1 }),
              );
            }
          }

          // Change state movement
          setMovementNumber(movementNumber + 1);
        } else {
          // Wrong key pressed
          setResultAction('wrong');
        }
      }
    },
    [user, race, movementNumber, coordinatesKeysMemo, raceActions, userActions],
  );

  // next movement (key)
  useEffect(() => {
    setCurrentKey(coordinatesKeysMemo[movementNumber].key);
  }, [coordinatesKeysMemo, movementNumber]);

  // Reset resultAction
  useEffect(() => {
    if (resultAction !== '') {
      setTimeout(() => {
        setResultAction('');
      }, 250);
    }
  }, [resultAction, setResultAction]);

  return (
    <div className={`map-actions ${resultAction}`}>
      {/* Waiting to start race */}
      {race.status === RACE_STATUS_PENDING && (
        <h3 className="map-actions__title">Waiting to start race</h3>
      )}
      {/* CountDown */}
      {race.status === RACE_STATUS_RUNNING && (
        <AppCountDown
          seconds={seconds}
          handlerFinishCountDown={handlerCountDown}
        />
      )}
      {/* Show keys */}
      {showKeys && race.status !== RACE_STATUS_FINISHED ? (
        <>
          <AppAddEvent
            context={windowMemo}
            event="keydown"
            handlerEvent={handlerKeyDown}
          />
          <AppKeyboardKeys keyboardKey={currentKey} />
        </>
      ) : null}
      {/* Finish Race */}
      {race.status === RACE_STATUS_FINISHED ? (
        <h3 className="map-actions__title">Finished race</h3>
      ) : null}
    </div>
  );
};

export default RaceMovements;
