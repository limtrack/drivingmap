export const RESET_USER = 'RESET_USER';
export const SET_USER = 'SET_USER';
export const SET_USER_ASYNC = 'SET_USER_ASYNC'; // saga action

interface ResetUserAction {
  type: typeof RESET_USER;
}

interface SetUserAction {
  type: typeof SET_USER;
  payload: object;
}

interface SetUserAsyncAction {
  type: typeof SET_USER_ASYNC;
  payload: object;
}

export type UserActionTypes =
  | ResetUserAction
  | SetUserAction
  | SetUserAsyncAction;
