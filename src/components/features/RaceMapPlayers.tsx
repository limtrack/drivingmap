import React, { useEffect, useState } from 'react';
// Contants
import { RACE_STATUS_PENDING, RACE_STATUS_RUNNING } from '../../constants';
// Hooks
import usePrevious from '../hooks/UsePrevious';
// Models
import { RaceType, RacePlayer } from '../../lib/firebase/models/race';
// Types
type Props = {
  gMap: any;
  race: RaceType;
};

const RaceMapPlayers: React.FC<Props> = ({ gMap, race }: Props): null => {
  // players
  const prevNumPlayers = usePrevious(race.players.length);
  // state
  const [playerMarks, setPlayerMarks] = useState([]);
  // Draw markers in initial position and clean
  useEffect(() => {
    const cleanPlayerMarks = (): any => {
      playerMarks.length &&
        playerMarks.forEach((mark: any) => {
          mark.gmMark.setMap(null);
        });
    };
    const drawPlayerMarks = (): any => {
      setPlayerMarks(
        race.players.reduce((sumMarks: any, player: RacePlayer, index) => {
          const currentCoordinate = race.coordinatesKeys[player.movements];
          // To show the player markers falling at different moments
          setTimeout(() => {
            sumMarks.push({
              id: player.id,
              gmMark: new google.maps.Marker({
                title: player.name,
                icon: `/images/${player.character}-small.png`,
                position: {
                  lat: currentCoordinate.lat,
                  lng: currentCoordinate.lng,
                },
                map: gMap,
                animation: google.maps.Animation.DROP,
              }),
            });
          }, index * 350);

          return sumMarks;
        }, []),
      );
    };

    if (
      race.status === RACE_STATUS_PENDING &&
      prevNumPlayers &&
      prevNumPlayers !== race.players.length
    ) {
      cleanPlayerMarks();
    }

    if (
      race.status === RACE_STATUS_PENDING &&
      (!prevNumPlayers || prevNumPlayers !== race.players.length)
    ) {
      drawPlayerMarks();
    }
  }, [race, gMap, prevNumPlayers]);

  // Move the markers
  useEffect(() => {
    const movePlayerMarks = (): any => {
      playerMarks.length &&
        playerMarks.forEach((mark: any) => {
          const currentPlayer = race.players.find((p): boolean => {
            return mark.id === p.id;
          });

          if (currentPlayer) {
            mark.gmMark.setPosition({
              lat: race.coordinatesKeys[currentPlayer.movements].lat,
              lng: race.coordinatesKeys[currentPlayer.movements].lng,
            });
          }
        });
    };

    if (race.status === RACE_STATUS_RUNNING) {
      movePlayerMarks();
    }
  }, [race, playerMarks]);

  return null;
};

export default React.memo(RaceMapPlayers);
