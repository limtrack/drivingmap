import React from 'react';
// Redux
import { useSelector } from 'react-redux';
// Types
import { StateType } from '../../../store/state/types';
// Components
import UserInfo from '../../features/UserInfo';

const AppHeader: React.FC = (): JSX.Element => {
  // State
  const user = useSelector((state: StateType) => state.user);

  return (
    <div className="header mb-3">
      <h1 className={`header__brand ${!user.id ? 'text-center w-100' : ''}`}>
        DrivingMap
      </h1>
      <UserInfo user={user} />
    </div>
  );
};

export default AppHeader;
