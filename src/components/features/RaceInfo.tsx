import React, { useMemo, useEffect, useCallback } from 'react';
// Models
import { RaceType, watchCurrentRace } from '../../lib/firebase/models/race';
import { PlayerType } from '../../lib/firebase/models/player';
// Hooks
import useActions from '../hooks/UseActions';
// Actions
import * as RaceActions from '../../store/race/actions';
// Components
import RaceMovements from './RaceMovements';
import RaceMap from './RaceMap';
import RacePlayers from './RacePlayers';

// Types
type Props = {
  race: RaceType;
  user: PlayerType;
};

const RaceInfo: React.FC<Props> = ({ race, user }: Props): JSX.Element => {
  // actions
  const raceActions = useActions(RaceActions);
  // memo
  const memoRaceMovements = useMemo(() => {
    return <RaceMovements race={race} user={user} />;
  }, [race, user]);

  // Handler to watch current race
  const handlerWatchCurrentRace = useCallback(
    (querySnapshot: any): void => {
      querySnapshot.exists &&
        raceActions.setRace({
          id: querySnapshot.id,
          ...querySnapshot.data(),
        });
    },
    [raceActions],
  );

  // Subscribe / unsubscribe to watch new races
  useEffect(() => {
    if (race.id) {
      const idSubscribe = watchCurrentRace(race.id).onSnapshot(
        handlerWatchCurrentRace,
      );

      return (): void => {
        idSubscribe();
      };
    }
  }, [race.id, handlerWatchCurrentRace]);

  return (
    <div className="race h-100">
      {race.id ? (
        <>
          <div className="race__players-list h-100 p-3">
            <RacePlayers race={race} />
          </div>
          <div className="race__map-race h-100">
            <div className="race__map-wrapper">
              <RaceMap race={race} />
            </div>
            <div className="race__map-actions">{memoRaceMovements}</div>
          </div>
        </>
      ) : (
        <div className="race__info h-100">
          <h2 className="race__info__title">You must select a race</h2>
        </div>
      )}
    </div>
  );
};

export default RaceInfo;
