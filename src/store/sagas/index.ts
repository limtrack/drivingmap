import { all } from 'redux-saga/effects';
// Sagas
import { watchSetUserData } from './user';
import { watchSetRaceData } from './race';

// notice how we now only export the rootSaga
// single entry point to start all Sagas at once
export default function* rootSaga() {
  yield all([watchSetUserData(), watchSetRaceData()]);
}
