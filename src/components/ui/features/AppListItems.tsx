import React from 'react';
// Transitions css
import { CSSTransition, TransitionGroup } from 'react-transition-group';

// Types
type Props = {
  title: string;
  subtitle: string;
  data: any[];
  render: (item: any) => any;
  titleAction?: () => JSX.Element;
};

const AppListItems: React.FC<Props> = ({
  title = 'Items',
  subtitle = 'All data',
  data = [],
  render,
  titleAction,
}: Props): JSX.Element => {
  return (
    <div className="items mb-3">
      {/* Header */}
      <div className="items__header">
        <div className="items__header-left">
          <h2 className="items__title mb-2">{title}</h2>
          {data.length ? (
            <h3 className="items__subtitle mb-3">{subtitle}</h3>
          ) : null}
        </div>
        <div className="items__header-right">
          {typeof titleAction !== 'undefined' && titleAction()}
        </div>
      </div>
      {/* List */}
      {data.length ? (
        <div className="items__content">
          <ul className="items__list">
            <TransitionGroup>
              {data.map((item: any, index: number) => {
                return (
                  <CSSTransition
                    key={index}
                    timeout={500}
                    classNames="items__list__item-transition"
                  >
                    {render(item)}
                  </CSSTransition>
                );
              })}
            </TransitionGroup>
          </ul>
        </div>
      ) : (
        <h3 className="items__nolist">There are not data</h3>
      )}
    </div>
  );
};

export default AppListItems;
