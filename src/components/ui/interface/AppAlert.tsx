import React from 'react';
// Redux
import { useSelector } from 'react-redux';
// Types
import { StateType } from '../../../store/state/types';
// Actions
import * as AlertActions from '../../../store/alert/actions';
// Hooks
import useActions from '../../hooks/UseActions';
// Bootstrap
import Alert from 'react-bootstrap/Alert';

const AppAlert: React.FC = (): JSX.Element | null => {
  const alert = useSelector((state: StateType) => state.alert);
  const alertActions = useActions(AlertActions);

  /**
   * close alert
   */
  const handleClose = (): void => {
    alertActions.resetAlert();
  };

  return alert.text ? (
    <Alert
      className="app-alert"
      dismissible={alert.dismissible}
      onClose={handleClose}
      show={alert.visible}
      variant="primary"
    >
      {alert.text}
    </Alert>
  ) : null;
};

export default AppAlert;
