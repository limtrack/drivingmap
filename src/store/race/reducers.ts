// Types
import { SET_RACE, RESET_RACE, RaceActionTypes } from './types';
// State
import initialState from '../state/initialState';

const { race } = initialState;

export default (state: object = race, action: RaceActionTypes): object => {
  switch (action.type) {
    case RESET_RACE: {
      return { ...race };
    }
    case SET_RACE: {
      return {
        ...state,
        ...action.payload,
      };
    }
    default: {
      return state;
    }
  }
};
