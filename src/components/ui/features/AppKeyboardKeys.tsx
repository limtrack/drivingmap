import React from 'react';
// Contants
import { KEYBOARD_KEYS } from '../../../constants';
// Font awesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IconProp } from '@fortawesome/fontawesome-svg-core';

// Types
type Props = {
  keyboardKey: string | undefined;
};

const AppKeyboardKeys: React.FC<Props> = ({
  keyboardKey,
}: Props): JSX.Element | null => {
  const currentKey = KEYBOARD_KEYS.find((k) => {
    return k.key === keyboardKey;
  });

  return currentKey && keyboardKey ? (
    <div className="keyboard-actions">
      <FontAwesomeIcon
        icon={currentKey.icon as IconProp}
        className="keyboard-actions__arrow"
      />
      <span className="keyboard-actions__label">{currentKey.label}</span>
    </div>
  ) : null;
};

export default AppKeyboardKeys;
