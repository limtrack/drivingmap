import React, { useCallback } from 'react';
// Firebase
import firebase from 'firebase/app';
// Contants
import {
  COUNTDOWN_RACE,
  RACE_STATUS_PENDING,
  RACE_STATUS_RUNNING,
} from '../../constants';
// Models
import { RaceType } from '../../lib/firebase/models/race';
// Components
import AppListItems from '../ui/features/AppListItems';
import PlayerItem from './PlayerItem';
// Bootstrap
import Button from 'react-bootstrap/Button';
// Hooks
import useActions from '../../components/hooks/UseActions';
// Actions
import * as RaceActions from '../../store/race/actions';
// Font awesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

// Types
type Props = {
  race: RaceType;
};

const RacePlayers: React.FC<Props> = ({ race }: Props): JSX.Element => {
  // actions
  const raceActions = useActions(RaceActions);

  const startRace = useCallback((): void => {
    const raceDate = new Date();

    // Add seconds to race date
    raceDate.setSeconds(raceDate.getSeconds() + COUNTDOWN_RACE);
    raceActions.setRaceAsync(
      Object.assign(race, {
        status: RACE_STATUS_RUNNING,
        date: firebase.firestore.Timestamp.fromDate(raceDate),
      }),
    );
  }, [race, raceActions]);

  // Show action button in title
  const handlerTitleAction = useCallback(() => {
    return (
      <Button
        disabled={race.status !== RACE_STATUS_PENDING}
        variant="primary"
        onClick={startRace}
      >
        <FontAwesomeIcon icon="flag-checkered" />
      </Button>
    );
  }, [race, startRace]);

  return (
    <AppListItems
      data={race.players}
      title="Players"
      titleAction={handlerTitleAction}
      subtitle="All participants"
      render={(item: any): any => {
        return <PlayerItem item={item} />;
      }}
    />
  );
};

const shouldNotUpdate = (prevProps: any, nextProps: any): boolean => {
  return nextProps.race.status !== RACE_STATUS_PENDING;
};

export default React.memo(RacePlayers, shouldNotUpdate);
