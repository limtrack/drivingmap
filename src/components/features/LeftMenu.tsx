import React from 'react';
// Font awesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// Hooks
import useActions from '../hooks/UseActions';
// Components
import AppAuthor from '../ui/interface/AppAuthor';
// Actions
import * as ModalActions from '../../store/modal/actions';

const LeftMenu: React.FC = (): JSX.Element => {
  const modalActions = useActions(ModalActions);

  // Show modal with information about me
  const showModalUser = (): void => {
    modalActions.setModal({
      title: 'About me',
      content: AppAuthor,
      showAcceptButton: false,
      textCancelButton: 'Close',
      visible: true,
    });
  };

  return (
    <div className="left-menu__list h-100">
      <span className="left-menu__item" onClick={showModalUser}>
        <FontAwesomeIcon icon="user-circle" className="left-menu__item-text" />
      </span>
      <a
        className="left-menu__item"
        href="https://bitbucket.org/limtrack/drivingmap/src/master/"
      >
        <FontAwesomeIcon icon="code" className="left-menu__item-text" />
      </a>
    </div>
  );
};

export default React.memo(LeftMenu);
