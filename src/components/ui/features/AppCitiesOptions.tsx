import React from 'react';
// Data
import cities from '../../../data/cities.js';

// Types
type City = {
  city: string;
  admin: string;
  country: string;
  populationProper: number;
  iso2: string;
  capital: string;
  lat: number;
  lng: number;
  population: number;
};

const AppCitiesOptions: React.FC = (): JSX.Element => {
  const citiesOrdered = cities.sort((a, b) => {
    return a.city.localeCompare(b.city, 'es');
  });

  return (
    <>
      {citiesOrdered.map((item: City, index: number) => {
        return (
          <option key={index} value={`${item.city}::${item.lat}::${item.lng}`}>
            {item.city}
          </option>
        );
      })}
    </>
  );
};

export default AppCitiesOptions;
