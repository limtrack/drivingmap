export const DISJOIN_PLAYER_ASYNC = 'DISJOIN_PLAYER_ASYNC';
export const JOIN_PLAYER_ASYNC = 'JOIN_PLAYER_ASYNC';
export const SET_RACE = 'SET_RACE';
export const SET_RACE_ASYNC = 'SET_RACE_ASYNC';
export const RESET_RACE = 'RESET_RACE';

interface DisjoinPlayerActionAsync {
  type: typeof DISJOIN_PLAYER_ASYNC;
  payload: object;
}

interface JoinPlayerActionAsync {
  type: typeof JOIN_PLAYER_ASYNC;
  payload: object;
}
interface ResetRaceAction {
  type: typeof RESET_RACE;
}

interface SetRaceAction {
  type: typeof SET_RACE;
  payload: object;
}

interface SetRaceActionAsync {
  type: typeof SET_RACE_ASYNC;
  payload: object;
}

export type RaceActionTypes =
  | DisjoinPlayerActionAsync
  | JoinPlayerActionAsync
  | ResetRaceAction
  | SetRaceAction
  | SetRaceActionAsync;
