import React from 'react';
// Models
import { RacePlayer } from '../../lib/firebase/models/race';
// Components
import AppCharacterPlayer from '../ui/interface/AppCharacterPlayer';

// Types
type Props = {
  item: RacePlayer;
};

const PlayerItem: React.FC<Props> = ({ item }: Props): JSX.Element => {
  return (
    <li className="items__list__item">
      <div className="items__list__item-left items__list__item-left--players w-100">
        <AppCharacterPlayer characterId={item.character} width={38} />
        <span className="items__list__item-name">{item.name}</span>
      </div>
    </li>
  );
};

export default PlayerItem;
