// Types
import { SET_RACE, RESET_RACE } from '../race/types';
import { SET_ALERT } from '../alert/types';
// Models
import { addRace } from '../../lib/firebase/models/race';
// Saga
import { takeLatest, put } from 'redux-saga/effects';

/**
 * Handler error (show error alert)
 */
function* handlerError(message: string): any {
  // Show error
  yield put({
    type: SET_ALERT,
    payload: {
      text: message || 'There was an error applying changes in race',
      type: 'danger',
    },
  });
}

/**
 * Set new race data in server (firebase)
 */
function* setRaceAsync({ payload }: any): any {
  try {
    // Save in server
    const raceData = yield addRace(payload);
    const raceId = raceData && raceData.id ? raceData.id : payload.id || null;
    // Change state in store
    yield put({
      type: SET_RACE,
      payload: { id: raceId, ...payload },
    });
  } catch (err) {
    // Show error
    handlerError(err.message);
  }
}

/**
 * Join user in race data in server (firebase)
 */
function* joinPlayerAsync({ payload }: any): any {
  try {
    // Save in server
    yield addRace(payload);

    // Change state in store
    yield put({
      type: SET_RACE,
      payload,
    });
  } catch (err) {
    // Show error
    handlerError(err.message);
  }
}

/**
 * Disjoin user in race data in server (firebase)
 */
function* disjoinPlayerAsync({ payload }: any): any {
  try {
    // Save in server
    yield addRace(payload);

    // disjoin race
    yield put({
      type: RESET_RACE,
    });
  } catch (err) {
    // Show error
    handlerError(err.message);
  }
}

// Watcher functions
export function* watchSetRaceData(): any {
  yield takeLatest('SET_RACE_ASYNC', setRaceAsync);
  yield takeLatest('JOIN_PLAYER_ASYNC', joinPlayerAsync);
  yield takeLatest('DISJOIN_PLAYER_ASYNC', disjoinPlayerAsync);
}
