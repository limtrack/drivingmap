export default {
  // Model (firebase)
  user: {
    id: '',
    character: '',
    name: 'No name',
    victories: 0,
  },
  race: {
    id: '',
    date: null,
    distanceBetweenCities: 0,
    from: [], // coordinates
    fromCity: null,
    to: [], // coordinates
    toCity: null,
    players: [],
    coordinatesKeys: [],
    status: 'pending',
    winningPlayer: null,
  },
  // UI
  loading: {
    text: 'Loading...',
    visible: false,
  },
  modal: {
    actionAcceptButton: null,
    actionCancelButton: null,
    backdrop: true,
    title: 'Title Modal',
    content: '',
    showHeader: true,
    showFooter: true,
    showAcceptButton: true,
    showCancelButton: true,
    showCloseButton: true,
    textAcceptButton: 'Accept',
    textCancelButton: 'Cancel',
    visible: false,
  },
  alert: {
    dismissible: false,
    duration: 3000,
    text: '',
    type: 'warning',
    visible: false,
  },
};
