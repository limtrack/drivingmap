import React, { useState, useEffect } from 'react';
// Contants
import { KEYBOARD_KEYS } from '../../constants';
// Firebase
import firebase from 'firebase/app';
// Redux
import { useSelector } from 'react-redux';
// Types
import { StateType } from '../../store/state/types';
// Hooks
import { useForm } from 'react-hook-form';
import useActions from '../hooks/UseActions';
// Bootstrap
import Form from 'react-bootstrap/Form';
import AppButtonLoading from '../ui/interface/AppButtonLoading';
// Components
import AppCitiesOptions from '../ui/features/AppCitiesOptions';
// Actions
import * as RaceActions from '../../store/race/actions';
import * as ModalActions from '../../store/modal/actions';
// Utils
import { getRouteBetweenCities, generateRandomArray } from '../../lib/utils';

const get = require('lodash.get');

const RaceForm: React.FC = (): JSX.Element => {
  // state
  const user = useSelector((state: StateType) => state.user);
  const race = useSelector((state: StateType) => state.race);
  const [isLoading, setLoading] = useState(false);
  // form validator
  const { handleSubmit, getValues, errors, register } = useForm();
  // actions
  const raceActions = useActions(RaceActions);
  const modalActions = useActions(ModalActions);

  // Hide modal when race change (the race was created and setted)
  useEffect(() => {
    if (race.id) {
      modalActions.setModal({ visible: false });
    }
  }, [race.id, modalActions]);

  /**
   * handle submit form
   */
  const onSubmit = (data: Record<string, any>): void => {
    const { from, to } = data;
    const [fromCity, fromLat, fromLng] = from.split('::');
    const [toCity, toLat, toLng] = to.split('::');
    const floatFromLat = Number.parseFloat(fromLat);
    const floatFromLng = Number.parseFloat(fromLng);
    const floatToLat = Number.parseFloat(toLat);
    const floatToLng = Number.parseFloat(toLng);

    setLoading(true);

    // Get path from route
    getRouteBetweenCities(
      {
        lat: floatFromLat,
        lng: floatFromLng,
      },
      {
        lat: floatToLat,
        lng: floatToLng,
      },
    )
      .then((response) => {
        const overviewPath = get(response, 'routes[0].overview_path', false);
        const availableKeys = KEYBOARD_KEYS.reduce((sumKeys: string[], k) => {
          sumKeys.push(k.key);
          return sumKeys;
        }, []);

        if (overviewPath === false) {
          throw new Error('It was impossible to get the path');
        }

        const distance = get(response, 'routes[0].legs[0].distance.value', 0); //metres
        const randomKeys = generateRandomArray(
          availableKeys,
          overviewPath.length,
        );

        // Create race
        raceActions.setRaceAsync({
          coordinatesKeys: overviewPath.map(
            (item: any, index: number): object => {
              return {
                lat: item.lat(),
                lng: item.lng(),
                key: randomKeys ? randomKeys[index] : null,
              };
            },
          ),
          distanceBetweenCities: Math.ceil(distance / 1000), // to Kms,
          from: new firebase.firestore.GeoPoint(floatFromLat, floatFromLng),
          fromCity: fromCity,
          to: new firebase.firestore.GeoPoint(floatToLat, floatToLng),
          toCity: toCity,
          players: [
            {
              id: user.id,
              name: user.name,
              character: user.character,
              movements: 0,
            },
          ],
        });
      })
      .catch(() => {
        // TODO ERROR
      })
      .finally(() => setLoading(false));
  };

  return (
    <Form onSubmit={handleSubmit(onSubmit)}>
      <Form.Group controlId="formFrom">
        <Form.Label>From</Form.Label>
        <Form.Control
          as="select"
          isInvalid={!!errors.from}
          name="from"
          ref={
            register({
              required: { value: true, message: 'Required field' },
            }) as RBRef
          }
        >
          <AppCitiesOptions />
        </Form.Control>
        {/* Show errors */}
        {errors.from && (
          <Form.Control.Feedback type="invalid">
            {(errors['from'] as any)?.message}
          </Form.Control.Feedback>
        )}
      </Form.Group>

      <Form.Group controlId="formTo">
        <Form.Label>To</Form.Label>
        <Form.Control
          as="select"
          isInvalid={!!errors.to}
          name="to"
          ref={
            register({
              required: { value: true, message: 'Required field' },
              validate: {
                sameCities: (value): any => {
                  const { from } = getValues();
                  return from === value
                    ? 'The source city and destiny city must be different'
                    : null;
                },
              },
            }) as RBRef
          }
        >
          <AppCitiesOptions />
        </Form.Control>
        {/* Show errors */}
        {errors.to && (
          <Form.Control.Feedback type="invalid">
            {(errors['to'] as any)?.message}
          </Form.Control.Feedback>
        )}
      </Form.Group>

      <AppButtonLoading
        isLoading={isLoading}
        label="Create"
        labelLoading="Creating..."
      />
    </Form>
  );
};

export default RaceForm;
