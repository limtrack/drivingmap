/**
 * Get a random array with the indicated options
 *
 * @param options options to use to generate the array
 * @param times number of elemets that will be included in the array
 */
export const generateRandomArray = (
  options: (string | number)[],
  times: number,
): void | (string | number)[] => {
  if (!times) return;

  const selectedOptions: (string | number)[] = [];
  for (let i = 0; i < times; i++) {
    selectedOptions.push(options[Math.floor(Math.random() * options.length)]);
  }

  return selectedOptions;
};

/**
 * Get direction information from route
 *
 * @param origin Latitude and longitude origin place
 * @param destination Latitude and longitude destination place
 */
export const getRouteBetweenCities = (
  origin: object,
  destination: object,
): Promise<any> => {
  return new Promise(function(resolve, reject) {
    if (!google) {
      reject(new Error('Google maps library was not loaded'));
    }

    const directionsService = new google.maps.DirectionsService();

    directionsService.route(
      {
        origin,
        destination,
        travelMode: google.maps.TravelMode['DRIVING'],
      },
      (response, status) => {
        if (status !== google.maps.DirectionsStatus.OK) {
          reject(new Error('Error in the request to the server'));
        }

        return resolve(response);
      },
    );
  });
};

/**
 * Get direction renderer (google map service)
 */
export const getDirectionsRenderer = (): any => {
  if (!google) {
    new Error('Google maps library was not loaded');
  }

  return new google.maps.DirectionsRenderer();
};
