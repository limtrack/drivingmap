import { PlayerType } from '../../lib/firebase/models/player';
import { RaceType } from '../../lib/firebase/models/race';

export interface LoadingType {
  text: string;
  visible: boolean;
}

export interface ModalType {
  actionAcceptButton: () => void;
  actionCancelButton: () => void;
  backdrop: 'static' | boolean;
  title: string;
  content: any;
  showHeader: boolean;
  showFooter: boolean;
  showAcceptButton: boolean;
  showCancelButton: boolean;
  showCloseButton: boolean;
  textAcceptButton: string;
  textCancelButton: string;
  visible: boolean;
}

export interface AlertType {
  dismissible: boolean;
  duration: number;
  text: string;
  type?: string;
  visible: boolean;
}

export interface StateType {
  alert: AlertType;
  loading: LoadingType;
  modal: ModalType;
  user: PlayerType;
  race: RaceType;
}
