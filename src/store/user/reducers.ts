// Types
import { SET_USER, RESET_USER, UserActionTypes } from './types';
// State
import initialState from '../state/initialState';

const { user } = initialState;

export default (state: object = user, action: UserActionTypes): object => {
  switch (action.type) {
    case RESET_USER: {
      return { ...user };
    }
    case SET_USER: {
      return {
        ...state,
        ...action.payload,
      };
    }
    default: {
      return state;
    }
  }
};
