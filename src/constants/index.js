export const COUNTDOWN_RACE = 10;
export const DEFAULT_MAP_REGION = {
  latitude: 40.4165,
  longitude: -3.70256,
};
export const FIREBASE_CONFIG = {
  apiKey: 'AIzaSyCWSKPcxm8f8IIBb7MOGBeoXUATXh4r1Z0',
  authDomain: 'drivingmap-d6fba.firebaseapp.com',
  databaseURL: 'https://drivingmap-d6fba.firebaseio.com',
  projectId: 'drivingmap-d6fba',
  storageBucket: 'drivingmap-d6fba.appspot.com',
  messagingSenderId: '770281077920',
  appId: '1:770281077920:web:1b1f9c1d40c8680658ddc5',
  measurementId: 'G-W81NNVXF3G',
};
export const GOOGLE_MAP_API = 'AIzaSyBWvjSUzb6JECy1PqziGuBMm0LlbQQhJfE';
export const KEYBOARD_KEYS = [
  {
    key: 'ArrowUp',
    icon: 'arrow-alt-circle-up',
    label: 'Front',
  },
  {
    key: 'ArrowDown',
    icon: 'arrow-alt-circle-down',
    label: 'Back',
  },
  {
    key: 'ArrowLeft',
    icon: 'arrow-alt-circle-left',
    label: 'Left',
  },
  {
    key: 'ArrowRight',
    icon: 'arrow-alt-circle-right',
    label: 'Right',
  },
]; // associted to keys
export const NUM_MAX_MOVEMENTS = 50;
export const REDUX_KEY = 'drivingApp';
export const REQUEST_BY_MOVEMENTS = 5;
export const RACE_STATUS_PENDING = 'pending';
export const RACE_STATUS_RUNNING = 'running';
export const RACE_STATUS_FINISHED = 'finished';
