import React from 'react';
// Components
import AppCharacterPlayer from '../ui/interface/AppCharacterPlayer';
// Models
import { PlayerType } from '../../lib/firebase/models/player';
// Types
type Props = {
  user: PlayerType;
};

const UserInfo: React.FC<Props> = ({ user }: Props): JSX.Element | null => {
  return user && user.id ? (
    <div className="header__user-info">
      <div className="header__user-info__user-data">
        <h5 className="header__user-info__name">{user.name}</h5>
        <span className="header__user-info__victories">
          <strong>{user.victories}</strong> victories
        </span>
      </div>
      <AppCharacterPlayer characterId={user.character} />
    </div>
  ) : null;
};

export default UserInfo;
