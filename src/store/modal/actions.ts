import { SET_MODAL, RESET_MODAL, ModalActionTypes } from './types';

// TypeScript infers that this function is returning ResetModalAction
export function resetModal(): ModalActionTypes {
  return {
    type: RESET_MODAL,
  };
}

// TypeScript infers that this function is returning SetModalAction
export function setModal(payload: object): ModalActionTypes {
  return {
    type: SET_MODAL,
    payload,
  };
}
