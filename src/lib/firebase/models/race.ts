// Firebase
import firebase from 'firebase/app';
import { fs } from '../index';
// Contants
import { RACE_STATUS_PENDING } from '../../../constants';

// Types
export type RacePlayer = {
  id: string | undefined;
  name: string;
  character: string;
  movements: number;
};

export type CoordinatesKey = {
  lat: number;
  lng: number;
  key: string;
};

// Model
export interface RaceType {
  id: string;
  date: firebase.firestore.Timestamp | null;
  distanceBetweenCities: number;
  from: firebase.firestore.GeoPoint;
  fromCity: string;
  to: firebase.firestore.GeoPoint;
  toCity: string;
  players: Array<RacePlayer>;
  coordinatesKeys: Array<CoordinatesKey>;
  status: string;
  winningPlayer: string;
}

const defaultData = {
  date: null,
  distanceBetweenCities: 0, // kms
  from: new firebase.firestore.GeoPoint(37.392529, -5.994072), // Sevilla
  fromCity: 'Sevilla',
  to: new firebase.firestore.GeoPoint(40.4165, -3.70256), // Madrid
  toCity: 'Madrid',
  players: [],
  coordinatesKeys: [],
  status: RACE_STATUS_PENDING, // Values: "pending", "running" and "finished"
  winningPlayer: '',
};

// name collection in firebase
const nameCollection = 'races';

/**
 * Get the data model
 */
export const getDataModel = (): object => {
  return defaultData;
};

/**
 * Add race (update) in database
 */
export const addRace = async (data: RaceType): Promise<any> => {
  const currentData = Object.assign({}, getDataModel(), data);

  if (currentData.id) {
    // Update document
    const documentId = currentData.id;

    delete currentData.id;

    return await fs
      .collection(nameCollection)
      .doc(documentId)
      .update(currentData);
  } else {
    // Create new document
    return await fs.collection(nameCollection).add(currentData);
  }
};

/**
 * Listen to new races
 */
export const watchNewRaces = (): any => {
  return fs.collection(nameCollection);
};

/**
 * Listen changes (updates) in the current race
 */
export const watchCurrentRace = (raceId: string): any => {
  return fs.collection(nameCollection).doc(raceId);
};
