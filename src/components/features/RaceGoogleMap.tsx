import React, { useEffect, useRef } from 'react';
// Contants
import { DEFAULT_MAP_REGION } from '../../constants';

// Types
type Props = {
  onLoadMap: (fn: any) => any;
};

const RaceGoogleMap: React.FC<Props> = ({ onLoadMap }: Props): JSX.Element => {
  // wrapper map
  const wrapperMap = useRef<HTMLDivElement>(null);

  // load elements "google Maps" library
  useEffect(() => {
    onLoadMap((): any => {
      return new google.maps.Map(wrapperMap.current as Element, {
        center: {
          lat: DEFAULT_MAP_REGION.latitude,
          lng: DEFAULT_MAP_REGION.longitude,
        },
        disableDefaultUI: true,
        draggable: false,
        zoom: 5.7,
      });
    });
  });

  return <div ref={wrapperMap} className="map-race"></div>;
};

export default React.memo(RaceGoogleMap);
