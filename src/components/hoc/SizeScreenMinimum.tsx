import React, { useState, useEffect } from 'react';
// Types
type Props = {
  width?: number;
  height?: number;
};

const SizeScreenMinimum = ({
  width = 1200,
  height = 750,
  ...props
}: Props): any => (WrappedComponent: any): React.FC => {
  const SCM: React.FC = (): JSX.Element => {
    const [isEnoughLarge, setIsEnoughLarge] = useState(true);

    useEffect(() => {
      const eventResize = (): void => {
        setIsEnoughLarge(
          window.innerHeight >= height && window.innerWidth >= width,
        );
      };

      // every time that the windows is resize
      window.addEventListener('resize', eventResize, false);
      // the first time is loaded
      eventResize();

      return (): void => {
        window.removeEventListener('resize', eventResize);
      };
    }, []);

    return (
      <>
        {isEnoughLarge === false ? (
          <div className="error-message-full">
            {`Your need a resolution at least of ${width} x ${height} pixels`}
          </div>
        ) : (
          <WrappedComponent {...props} />
        )}
      </>
    );
  };

  return SCM;
};

export default SizeScreenMinimum;
