import React, { useState, useEffect } from 'react';

// Types
type Props = {
  className?: string;
  handlerFinishCountDown?: () => void;
  hideAfterFinishCountDown?: boolean;
  seconds: number;
  title?: string;
};

const AppCountDown: React.FC<Props> = ({
  className,
  handlerFinishCountDown,
  hideAfterFinishCountDown = true,
  seconds,
  title,
}: Props): JSX.Element | null => {
  const [secondsLeft, setSecondsLeft] = useState(seconds);
  const [hideCountDown, setHideCountDown] = useState(false);

  useEffect(() => {
    const timer =
      secondsLeft > 0 &&
      setInterval(() => {
        setSecondsLeft(secondsLeft - 1);
      }, 1000);

    if (secondsLeft <= 0) {
      handlerFinishCountDown && handlerFinishCountDown();
      hideAfterFinishCountDown && setHideCountDown(true);
      timer && clearInterval(timer);
    }

    return (): void => {
      timer && clearInterval(timer);
    };
  }, [handlerFinishCountDown, hideAfterFinishCountDown, secondsLeft]);

  return !hideCountDown ? (
    <div className={`count-down ${className}`}>
      {title ? <h3 className="count-down__title">{title}</h3> : null}
      <span className="count-down__seconds">{secondsLeft}</span>
    </div>
  ) : null;
};

export default AppCountDown;
