import React from 'react';
// Redux
import { useSelector } from 'react-redux';
// Types
import { StateType } from '../../../store/state/types';
// Actions
import * as ModalActions from '../../../store/modal/actions';
// Hooks
import useActions from '../../hooks/UseActions';
// Bootstrap
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';

const AppModal: React.FC = (): JSX.Element | null => {
  const modal = useSelector((state: StateType) => state.modal);
  const modalActions = useActions(ModalActions);
  const Content = modal.content;

  /**
   * hide modal
   */
  const handleHide = (): void => {
    modalActions.setModal({
      visible: false,
    });
  };

  /**
   * reset modal
   */
  const handleExited = (): void => {
    modalActions.resetModal();
  };

  /**
   * On click accept button
   */
  const onAcceptButton = (): void => {
    typeof modal.actionAcceptButton === 'function'
      ? modal.actionAcceptButton()
      : handleHide();
  };

  /**
   * On click cancel button
   */
  const onCancelButton = (): void => {
    typeof modal.actionCancelButton === 'function'
      ? modal.actionCancelButton()
      : handleHide();
  };

  return modal.content ? (
    <Modal
      centered
      backdrop={modal.backdrop}
      show={modal.visible}
      onHide={handleHide}
      onExited={handleExited}
    >
      {modal.showHeader ? (
        <>
          <Modal.Header closeButton={modal.showCloseButton}>
            <Modal.Title>{modal.title || 'Title modal'}</Modal.Title>
          </Modal.Header>
        </>
      ) : null}
      <Modal.Body>
        {typeof Content === 'string' ? Content : <Content />}
      </Modal.Body>
      {modal.showFooter ? (
        <>
          <Modal.Footer>
            {modal.showCancelButton ? (
              <Button variant="secondary" onClick={onCancelButton}>
                {modal.textCancelButton || 'Cancel'}
              </Button>
            ) : null}
            {modal.showAcceptButton ? (
              <Button variant="primary" onClick={onAcceptButton}>
                {modal.textAcceptButton || 'Accept'}
              </Button>
            ) : null}
          </Modal.Footer>
        </>
      ) : null}
    </Modal>
  ) : null;
};

export default AppModal;
